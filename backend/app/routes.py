from app import app
from elasticsearch import Elasticsearch
from flask import jsonify
import operator
from datetime import datetime, timedelta


INDEX_NAME = "active"
CHANNELS_NAME = "channels"
TOPICS_NAME = "topics"


def parse_date_time(date):

    dt_obj = datetime.strptime(date[:19], '%Y-%m-%dT%H:%M:%S')
    time_in = datetime.now() + timedelta(hours=2) - dt_obj

    return str(dt_obj)[:-3], str(time_in)[:-7]


def get_data():

    items = {}
    ret_list = []
    sorted_tweets = {}

    client = Elasticsearch(
        hosts=[{"host": "example_es"}], port=9200
    )

    # If index of active items is not created yet, we create it
    if not client.indices.exists(index=INDEX_NAME):
        return "No index", 200

    messages = client.search(
        index=INDEX_NAME, size=1500)
    newsSet = set()

    for message in messages['hits']['hits']:

        id = str(message['_id'])
        url = message['_source']['url']
        if url in newsSet:
            continue
        newsSet.add(url)

        tweets = message['_source']['twitter']
        title = message['_source']['title']
        date = message['_source']['date']

        dt_obj = datetime.strptime(date[:19], '%Y-%m-%dT%H:%M:%S')
        date = dt_obj.strftime("%d %b %Y")
        num_tweets = 0
        num_favs = 0

        for tweet in tweets:
            num_tweets += tweets[tweet][u'retweet']
            num_favs += tweets[tweet][u'favorites']
            for quoted_tweets in tweets[tweet][u'quoted_tweets']:
                num_favs += tweets[tweet][u'quoted_tweets'][quoted_tweets][u'favorites']
                num_tweets += tweets[tweet][u'quoted_tweets'][quoted_tweets][u'retweet']

        if num_tweets > 0 or num_favs > 0:

            try:
                values = items[dt_obj]
            except KeyError:
                values = items[dt_obj] = []
            data = {'id': id, 'url': url, 'num_rt': num_tweets,
                    'num_favs': num_favs, 'title': title, 'date': date}
            values.append(data)

    sorted_tweets = sorted(items.keys(), reverse=True)

    for i in sorted_tweets:
        for a in items[i]:
            ret_list.append(a)

    return jsonify(ret_list), 200


def get_old_data():

    items = {}
    ret_list = []
    sorted_tweets = {}

    client = Elasticsearch(
        hosts=[{"host": "example_es"}], port=9200
    )
    # If index of active items is not created yet, we create it
    if not client.indices.exists(index="no_" + INDEX_NAME):
        return "No index", 200

    messages = client.search(
        index="no_" + INDEX_NAME, size=1500)

    for message in messages['hits']['hits']:

        id = str(message['_id'])

        url = message[u'_source'][u'doc'][u'url']
        tweets = message[u'_source'][u'doc'][u'twitter']
        title = message[u'_source'][u'doc'][u'title']
        date = message[u'_source'][u'doc'][u'date']

        dt_obj = datetime.strptime(date[:19], '%Y-%m-%dT%H:%M:%S')
        date = dt_obj.strftime("%d %b %Y")
        num_tweets = 0
        num_favs = 0

        if len(tweets) == 0:
            try:
                values = items[dt_obj]
            except KeyError:
                values = items[dt_obj] = []
            data = {'id': id, 'url': url, 'num_rt': 0,
                    'num_favs': num_favs, 'title': title, 'date': date}
            values.append(data)

        for tweet in tweets:
            num_tweets += tweets[tweet][u'retweet']
            num_favs += tweets[tweet][u'favorites']
            for quoted_tweets in tweets[tweet][u'quoted_tweets']:
                num_favs += tweets[tweet][u'quoted_tweets'][quoted_tweets][u'favorites']
                num_tweets += tweets[tweet][u'quoted_tweets'][quoted_tweets][u'retweet']

            try:

                values = items[dt_obj]
            except KeyError:
                values = items[dt_obj] = []
            data = {'id': id, 'url': url, 'num_rt': num_tweets,
                    'num_favs': num_favs, 'title': title, 'date': date}
            values.append(data)

    sorted_tweets = sorted(items.keys(), reverse=True)

    for i in sorted_tweets:
        for a in items[i]:
            ret_list.append(a)

    return jsonify(ret_list), 200


def get_tweet_info(id):
    client = Elasticsearch(
        hosts=[{"host": "example_es"}], port=9200
    )
    # If index of active items is not created yet, we create it
    if not client.indices.exists(index=INDEX_NAME):
        return None

    try:
        message = client.get(index=INDEX_NAME, id=str(id))
    except:
        return None
    id_ret = message[u'_id']
    message = message[u'_source']

    tweets = message[u'twitter']
    channels = message[u'channel']
    date, time_in = parse_date_time(message[u'date'])

    num_favs = 0
    num_rt = 0

    message_ret = {}

    message_ret['num_channels'] = len(channels)
    channels_ret = list()

    for channel in channels:
        c = {}
        c["name"] = channel
        c["id"] = channels[channel]
        c["profile"] = "https://t.me/s/" + channel
        c["profile_image"] = "https://unavatar.io/telegram/" + channel
        channels_ret.append(c)

    message_ret['channels'] = channels_ret

    tweets_list = list()
    for tweet in tweets:
        id = tweet
        favs = tweets[tweet][u'favorites']
        profile_image = tweets[tweet][u'profile_image']
        name = tweets[tweet][u'name']
        rt = tweets[tweet][u'retweet']
        try:
            text = tweets[tweet][u'text']
            text = text.split("https://t.co/")[0]
        except:
            text = ""
        profile_url = "https://twitter.com/" + name

        tweet_link = "https://twitter.com/i/web/status/" + id

        num_favs += favs
        num_rt += rt

        q_list = list()
        for quoted_tweets in tweets[tweet][u'quoted_tweets']:
            num_rt += tweets[tweet][u'quoted_tweets'][quoted_tweets][u'retweet']
            num_favs += tweets[tweet][u'quoted_tweets'][quoted_tweets][u'favorites']

            id_quoted = quoted_tweets
            tweet_link_quoted = "https://twitter.com/i/web/status/" + id_quoted
            favs_quoted = tweets[tweet][u'quoted_tweets'][quoted_tweets][u'favorites']
            rt_quoted = tweets[tweet][u'quoted_tweets'][quoted_tweets][u'retweet']

            profile_image = tweets[tweet][u'profile_image']
            name = tweets[tweet][u'name']
            q = {"id": id_quoted, "favorites": favs_quoted, "retweet": rt_quoted,
                 "link": tweet_link_quoted, 'name': name, 'profile_image': profile_image}
            q_list.append(q)

        t = {"id": id, "favorites": favs, "retweet": rt, "quoted_tweets": q_list,
             "link": tweet_link, 'profile_image': profile_image, 'name': name,
             'text': text, 'profile_url': profile_url}
        tweets_list.append(t)

    message_ret['twitter'] = tweets_list
    message_ret['rt'] = num_rt
    message_ret['id'] = id_ret
    message_ret['favs'] = num_favs
    message_ret['url'] = message[u'url']
    message_ret['title'] = message[u'title']
    message_ret['date'] = date
    message_ret['time_in'] = time_in

    return message_ret


def get_old_tweet_info(id):
    client = Elasticsearch(
        hosts=[{"host": "example_es"}], port=9200
    )
    # If index of active items is not created yet, we create it
    if not client.indices.exists(index="no_" + INDEX_NAME):
        return None

    try:
        message = client.get(index="no_" + INDEX_NAME, id=str(id))
    except:
        return None

    id_ret = message[u'_id']
    message = message[u'_source'][u'doc']

    tweets = message[u'twitter']
    channels = message[u'channel']
    date, time_in = parse_date_time(message[u'date'])

    num_favs = 0
    num_rt = 0

    message_ret = {}

    message_ret['num_channels'] = len(channels)
    channels_ret = list()

    for channel in channels:
        c = {}
        c["name"] = channel
        c["id"] = channels[channel]
        c["profile"] = "https://t.me/s/" + channel
        c["profile_image"] = "https://unavatar.io/telegram/" + channel
        channels_ret.append(c)

    message_ret['channels'] = channels_ret

    tweets_list = list()
    for tweet in tweets:
        id = tweet
        favs = tweets[tweet][u'favorites']
        profile_image = tweets[tweet][u'profile_image']
        name = tweets[tweet][u'name']
        rt = tweets[tweet][u'retweet']
        text = tweets[tweet][u'text']
        tweet_link = "https://twitter.com/i/web/status/" + id
        profile_url = "https://twitter.com/" + name

        num_favs += favs
        num_rt += rt

        q_list = list()
        for quoted_tweets in tweets[tweet][u'quoted_tweets']:
            num_rt += tweets[tweet][u'quoted_tweets'][quoted_tweets][u'retweet']
            num_favs += tweets[tweet][u'quoted_tweets'][quoted_tweets][u'favorites']

            id_quoted = quoted_tweets
            tweet_link_quoted = "https://twitter.com/i/web/status/" + id_quoted
            favs_quoted = tweets[tweet][u'quoted_tweets'][quoted_tweets][u'favorites']
            rt_quoted = tweets[tweet][u'quoted_tweets'][quoted_tweets][u'retweet']
            profile_image = tweets[tweet][u'profile_image']
            name = tweets[tweet][u'name']
            q = {"id": id_quoted, "favorites": favs_quoted, "retweet": rt_quoted,
                 "link": tweet_link_quoted, 'name': name, 'profile_image': profile_image}
            q_list.append(q)

        t = {"id": id, "favorites": favs, "retweet": rt, "quoted_tweets": q_list,
             "link": tweet_link, 'profile_image': profile_image, 'name': name,
             'profile_url': profile_url, 'text': text}
        tweets_list.append(t)

    message_ret['twitter'] = tweets_list
    message_ret['rt'] = num_rt
    message_ret['id'] = id_ret
    message_ret['favs'] = num_favs
    message_ret['url'] = message[u'url']
    message_ret['title'] = message[u'title']
    message_ret['date'] = date

    return message_ret


def create_dict():

    periods = {}
    period = timedelta(hours=6)
    t = datetime.now() + timedelta(hours=2)

    for _ in range(7):
        date = datetime.strptime(
            str(t)[:-10], '%Y-%m-%d %H:%M').strftime('%d/%m/%Y %H:%M')
        periods[date] = 0
        t -= period

    return periods


def get_channels():

    data = dict([])
    period = timedelta(hours=24*7)
    period_48 = timedelta(hours=24*2)
    client = Elasticsearch(
        hosts=[{"host": "example_es"}], port=9200
    )

    # If index of active items is not created yet, we create it
    if not client.indices.exists(index=CHANNELS_NAME):
        return None

    try:
        message = client.search(index=CHANNELS_NAME, size=60)
    except:
        return None

    topics_channels = get_trending_topics_channels()
    for channel in message['hits']['hits']:
        news = channel["_source"]["news"]
        channel = channel["_source"]["channel"]
        last_week = 0
        active_news = 0

        for item in news:
            if news[item]['active'] == True:
                for date in news[item]['date_update']:
                    dt_obj = datetime.strptime(date[:19], '%Y-%m-%d %H:%M:%S')
                    t_last_week = datetime.now() + timedelta(hours=2) - period
                    t_last_48 = datetime.now() + timedelta(hours=2) - period_48

                    if t_last_48 < dt_obj:
                        active_news += 1

                    if t_last_week < dt_obj:
                        last_week += 1
                        break

        try:
            topics = topics_channels[channel]
            topics_channel = ""
            for topic in topics:
                topics_channel += topic + ", "
            topics_channel = topics_channel[:-2]

        except:
            topics_channel = ""

        if news == 0:
            try:
                values = data[0]
            except KeyError:
                values = data[0] = []
            values.append({'channel': channel,
                           "profile_image": "https://unavatar.io/telegram/" + str(channel),
                           "url": "https://t.me/s/" + str(channel),
                           "active_news": active_news, "topic_channel": topics_channel, "last_week": last_week})
            continue

        try:
            values = data[last_week]
        except KeyError:
            values = data[last_week] = []

        values.append({'channel': channel,
                       "profile_image": "https://unavatar.io/telegram/" + str(channel),
                       "url": "https://t.me/s/" + str(channel),
                       "active_news": active_news, "topic_channel": topics_channel, "last_week": last_week})

    sorted_channels = sorted(data.keys(), reverse=True)

    channel_lists = list()

    if len(sorted_channels) > 1:

        for i in sorted_channels:
            for a in data[i]:
                channel_lists.append(a)

    return jsonify(channel_lists), 200


def parse_date(date):

    dt_obj = datetime.strptime(date[:19], '%Y-%m-%dT%H:%M:%S')

    return str(dt_obj)[:-9]


def hour_ini():
    periods = {}
    period = timedelta(hours=6)
    t = datetime.now() + timedelta(hours=2)

    for i in range(7):
        periods[t] = 0
        t -= period

    return periods, sorted(periods.keys())


def statistics_48_hour(date):
    period = timedelta(hours=48)

    dt_obj = datetime.strptime(date[:19], '%Y-%m-%dT%H:%M:%S')
    t_48 = datetime.now() + timedelta(hours=2) - period

    if t_48 < dt_obj:
        return 1
    else:
        return 0


def statistics_hour(dic_hour, list_hour, date):

    dt_obj = datetime.strptime(date[:19], '%Y-%m-%dT%H:%M:%S')
    i = 0

    last_hour = list_hour[0]
    last_hour -= timedelta(hours=6)

    for hour in list_hour:
        if hour > dt_obj and dt_obj > last_hour:
            dic_hour[list_hour[i]] += 1
            break

        i += 1

    return dic_hour


def get_statistics():
    client = Elasticsearch(
        hosts=[{"host": "example_es"}], port=9200
    )

    # If index of active items is not created yet, we create it
    if not client.indices.exists(index=CHANNELS_NAME):
        return None

    try:
        channels_list = client.search(index=CHANNELS_NAME, size=60)
    except:
        return None

    # If index of active items is not created yet, we create it
    if not client.indices.exists(index=INDEX_NAME):
        return "No index", 200

    messages = client.search(
        index=INDEX_NAME, size=1500)

    channels = get_statistics_channels(client, channels_list, messages)

    return jsonify(channels), 200


def get_statistics_channels(client, channels_list, messages):
    channels = {}
    sorted_channels = list()
    dates = {}
    dic_hour, list_hour = hour_ini()

    old_tweets_48 = 0

    num_total_channels = len(channels_list['hits']['hits'])

    num_posts = 0

    for message in messages['hits']['hits']:
        num_posts += 1
        telegram = message['_source']['channel']
        tweets = message[u'_source'][u'twitter']
        date = message[u'_source'][u'date']
        dic_hour = statistics_hour(dic_hour, list_hour, date)
        old_tweets_48 += statistics_48_hour(date)
        date = parse_date(date)

        if date in dates:
            dates[date] += 1
        else:
            dates[date] = 1

        num_rt = 0

        for tweet in tweets:
            num_rt += tweets[tweet][u'retweet']

        if num_rt > 0:
            for channel in telegram:
                if channel is not "{}":
                    if str(channel) in channels:
                        channels[str(channel)] += 1
                    else:
                        channels[str(channel)] = 1

    # If index of active items is not created yet, we create it
    if not client.indices.exists(index="no_" + INDEX_NAME):
        return "No index", 200

    messages = client.search(
        index="no_" + INDEX_NAME, size=3000)

    for message in messages['hits']['hits']:
        num_posts += 1
        telegram = message[u'_source'][u'doc']['channel']
        tweets = message[u'_source'][u'doc'][u'twitter']
        date = message[u'_source'][u'doc'][u'date']
        date_delete = message[u'_source'][u'doc'][u'date_delete']
        dic_hour = statistics_hour(dic_hour, list_hour, date)
        old_tweets_48 += statistics_48_hour(date)
        date = parse_date(date)

        if date in dates:
            dates[date] += 1
        else:
            dates[date] = 1

        num_rt = 0

        for tweet in tweets:
            num_rt += tweets[tweet][u'retweet']

        if num_rt > 0:
            for channel in telegram:
                if channel is not "{}":

                    if str(channel) in channels:
                        channels[str(channel)] += 1
                    else:
                        channels[str(channel)] = 1

    for key, value in sorted(channels.items(), key=operator.itemgetter(1), reverse=True):
        data = {"name": key, "num": value}
        data["profile"] = "https://t.me/s/" + key
        data["profile_image"] = "https://unavatar.io/telegram/" + key
        sorted_channels.append(data)

    sorted_dates_list = list()
    sorted_dates_data = list()
    for key in sorted(dic_hour.keys()):

        date = datetime.strptime(
            str(key)[:-10], '%Y-%m-%d %H:%M').strftime('%d/%m/%Y %H:%M')
        sorted_dates_list.append(str(date))
        sorted_dates_data.append(dic_hour[key])

    num_channels = len(sorted_channels)

    percentage_active = 0
    if num_posts > 0:
        percentage_active = round((old_tweets_48/(num_posts + 0.0))*100)

    percentage_channels = round((num_channels/(num_total_channels + 0.0))*100)

    return {"num_channels": num_channels, "chart_data": sorted_dates_data, "chart_keys": sorted_dates_list,
            "num_active_posts": old_tweets_48,  "num_posts": num_posts,
            "channels": sorted_channels, "percentage_active": percentage_active, "percentage_channels": percentage_channels}


cache = {}


def get_trending_topics_channels():

    now = datetime.now()

    if 'topics_channels' in cache:
        cache_time = cache['topics_channels']['time']
        seconds = (now - cache_time).total_seconds()
        if seconds < (60*60*2):
            return cache['topics_channels']['data']

    data = {}

    client = Elasticsearch(
        hosts=[{"host": "example_es"}], port=9200
    )

    results = client.search(index="active",  size=10000)['hits']['hits']

    for res in results:

        try:

            channels = list(res['_source']['channel'].keys())

            term_vectors = client.termvectors(index="active",  id=res['_id'], offsets=True,
                                              payloads=True, positions=True, fields='text',
                                              field_statistics=True

                                              )['term_vectors']['text']['terms']

            for item in term_vectors:
                if int(term_vectors[item]['term_freq']) > 30:
                    if(len(item) > 4 and item.isalpha()):

                        for channel in channels:

                            try:
                                values = data[channel]
                            except KeyError:
                                values = data[channel] = []

                            values.append(item)

        except KeyError:
            pass

    cache['topics_channels'] = {'time': now, 'data': data}
    return data


def create_dict():

    periods = {}
    period = timedelta(hours=6)
    t = datetime.now() + timedelta(hours=2)

    for _ in range(7):
        date = datetime.strptime(
            str(t)[:-10], '%Y-%m-%d %H:%M').strftime('%d/%m/%Y %H:%M')
        periods[date] = 0
        t -= period

    return periods


def get_topics_48():

    topics_data = {}
    client = Elasticsearch(
        hosts=[{"host": "example_es"}], port=9200
    )

    created_dict = create_dict()
    list_dates = [0] * len(created_dict)

    # If index of active items is not created yet, we create it
    if not client.indices.exists(index=TOPICS_NAME):
        return None

    try:
        items = client.search(index=TOPICS_NAME, size=1000)['hits']['hits']
    except:
        return None

    for item in items:
        date = item['_source']['date']
        date = datetime.strptime(date[:19], '%Y-%m-%dT%H:%M:%S')
        topics = item['_source']['topics']

        for topic in topics:
            try:
                list_hour = topics_data[topic]
            except KeyError:
                list_hour = topics_data[topic] = list_dates.copy()

            i = 0
            for d in created_dict:
                date_dict = datetime.strptime(d, '%d/%m/%Y %H:%M')
                if date_dict < date:
                    list_hour[i-1] += 1
                    i = 0
                    break
                i += 1

    return topics_data, created_dict


def get_trending_topics():

    now = datetime.now()
    data = {}
    sorted_topics = {}
    topics = list()
    topics_48, dict_created = get_topics_48()

    client = Elasticsearch(
        hosts=[{"host": "example_es"}], port=9200
    )

    messages = client.search(index=TOPICS_NAME, body={
        "size": 1,
        "sort": {"date": "desc"},
        "query": {
            "match_all": {}
        }})['hits']['hits']

    for message in messages:
        for topic in message['_source']['topics']:
            topic_dict = message['_source']['topics'][topic][0]
            freq = topic_dict['freq']
            try:
                values = data[freq]
            except KeyError:
                values = data[freq] = []

            if topic in topics_48:
                chart_topics = topics_48[topic]
            else:
                chart_topics = dict_created

            values.append(
                {'topic': topic_dict['topic'], 'channels': topic_dict['channels'], 'chart_data': chart_topics[::-1]})

    sorted_topics = sorted(data.keys(), reverse=True)

    for i in sorted_topics:
        for a in data[i]:
            topics.append(a)

    return {'topics': topics, 'dates': list(dict_created.keys())}, 200


@app.route('/')
def index():

    return "", 200


@app.route('/statistics')
def statistics():

    ret = get_statistics()
    if ret is None:
        return "Internal Server Error", 500
    return ret


@app.route('/data')
def data():

    ret = get_data()
    if ret is None:
        return "Internal Server Error", 500
    else:
        return ret


@app.route('/old_data')
def old_data():

    ret = get_old_data()
    if ret is None:
        return "Internal Server Error", 500
    else:
        return ret


@app.route('/old/news/<id>', methods=['GET'])
def get_old_tweet(id):

    tweet = get_old_tweet_info(id)
    ret = list()
    ret.append(tweet)

    if tweet is None:
        return "Bad Request", 400
    return jsonify(ret), 200


@app.route('/news/<id>', methods=['GET'])
def get_tweet(id):

    tweet = get_tweet_info(id)
    ret = list()
    ret.append(tweet)

    if tweet is None:
        return "Bad Request", 400
    return jsonify(ret), 200


@app.route('/channels', methods=['GET'])
def channels():
    ret = get_channels()
    if ret is None:
        return "Internal Server Error", 500
    else:
        return ret


@app.route('/topics', methods=['GET'])
def topics():
    ret = get_trending_topics()
    if ret is None:
        return "Internal Server Error", 500
    else:
        return ret
