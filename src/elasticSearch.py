import argparse
import json
import logging
import sys
import threading
import time
from argparse import RawTextHelpFormatter
from collections import defaultdict
from datetime import datetime, timedelta
from json import loads
from configuration import *
from elasticsearch import Elasticsearch
from twitter import *
from topics import *
from title import get_title, get_text
from kafka import KafkaConsumer


logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',
                    filename='elasticSearch.log', filemode='a+',  level=logging.INFO)


# Global variables
client = None
config_data = None
config_data = None
mutex = threading.Lock()

# Parse of arguments
parser = argparse.ArgumentParser(
    description='Reading Telegram channel messages via channel URL',
    formatter_class=RawTextHelpFormatter)
parser.add_argument(
    '--config',
    dest='config',
    default=False,
    help='Configuration file')
args = parser.parse_args()

if args.config is False:
    parser.print_help()
    sys.exit(-1)

config_data = credentails(args.config)

# Conect to consumer , receive messages
consumer = KafkaConsumer(
    config_data['PIPELINE1'],
    bootstrap_servers=['kafka1:29092'],
    auto_offset_reset='earliest',
    enable_auto_commit=True,
    api_version=(0, 11, 5),
    value_deserializer=lambda x: loads(x.decode('utf-8')))


def combine_dicts(dict1, dict2):
    """
    Combines two dictionaries with lists as values.
    Two entries with same keys will be merge and the
    lists of the values will be merge into a new one
    in a set (without repeated elements)

    Parameters:
        dict1 -> dictionary 1
        dict2 -> dictionary 2

    Returns:
        Merged dictionary

   """
    de = defaultdict(list, dict1)
    for i, j in dict2.items():
        de[i].extend(j)
        de[i] = list(set(de[i]))

    return json.loads(json.dumps(de))


def create_index_channels(client, name):
    """
    Creates an index in Elasticsearch if one isn't already there.
    The mapping of the index will be:

                - channel: name of channel
                - news: total news

    Parameters:
       client -> elasticSearch client

   """
    logging.info("Creating index %s", name)
    client.indices.create(
        index=name,
        body={
            "settings": {
                "number_of_shards": 1,
                "index.mapping.total_fields.limit": 6000,
                "index.mapping.depth.limit": 100},
            "mappings": {

                "properties": {
                    "channel": {
                        "type": "text"},
                    "news": {
                        "type": "object"}}},
        },
        ignore=400,
    )

    add_channels(client)

    logging.info("Index created")


def create_index_topics(client, name):
    """
    Creates an index in Elasticsearch if one isn't already there.
    The mapping of the index will be:

                - topics: trending topics in a specific date
                - date: date and hour of the execution

    Parameters:
       client -> elasticSearch client

   """
    logging.info("Creating index %s", name)
    client.indices.create(
        index=name,
        body={
            "settings": {
                "number_of_shards": 1,
                "index.mapping.total_fields.limit": 8000,
                "index.mapping.depth.limit": 100},
            "mappings": {

                "properties": {
                    "topics": {
                        "type": "object"},
                    "date": {
                        "type": "date"},
                }},
        },
        ignore=400,
    )

    logging.info("Index created")


def add_channels_file(client, channels):
    """
    Adds all channels in urls file to the index

    Parameters:
        client ->   ElasticSearch client
        channels -> channels to add to ddbb
    """
    # For each channel
    for channel in channels:

        client.index(
            index=config_data['INDEX_CHANNELS'],
            body={
                'channel': str(channel),
                'news': {}})


def parse_channels(file, channels_set):
    """
    Parses channel name, getting only the channels from
    a URL

    Parameters:
        file ->   file with Telegram channels
        channels_set -> set of channels to add
    """

    # For each channel
    for channel in file:

        telegram_name = channel.split("/")[-1]

       # Avoid empty lines in file
        if len(telegram_name) <= 1:
            continue

        if telegram_name[-1] == "\n":
            telegram_name = telegram_name[:-1]

        if telegram_name[-1] == "\r":
            telegram_name = telegram_name[:-1]

        channels_set.add(telegram_name)

    return channels_set


def add_channels(client):
    """
    Adds all channels in urls file to the index

    Parameters:
        client -> ElasticSearch client
    """

    channels_set = set()

    # Read file with url of telegram channel
    try:
        f = open("urls/url.txt", "r")
    except FileNotFoundError:
        return None

    channels_set = parse_channels(f, channels_set)

    f.close()

    # Read file with url of telegram channel
    try:
        f = open("urls/url1.txt", "r")
    except FileNotFoundError:
        return None

    channels_set = parse_channels(f, channels_set)

    f.close()

    add_channels_file(client, channels_set)


def update_channels(client, channel, url):
    """
    Adds one to the total news of a channel in the channel index

    Parameters:
        client -> ElasticSearch client
        channel -> channel to be updated
        url -> url of the channel to be updated
    """

    messages = client.search(index=config_data['INDEX_CHANNELS'], body={
        "query": {"match": {"channel": str(channel)}}})

    if len(messages['hits']['hits']) != 0:
        id = None
        news = messages['hits']['hits'][0]['_source']['news']
        if url in news:
            news[url]['date_update'].append(
                str(datetime.now() + timedelta(hours=2)))
        else:
            dates = []
            dates.append(str(datetime.now() + timedelta(hours=2)))
            news[url] = {'active': True, 'date_update': dates}

        for list in messages['hits']['hits']:
            if channel == list['_source']['channel']:
                id = list['_id']
                break

        if id is not None:
            a = client.update(
                index=config_data['INDEX_CHANNELS'],
                id=id,
                body={"doc": {
                    'channel': str(channel),
                    'news': news}})


def create_index(client, name):
    """
    Creates an index in Elasticsearch if one isn't already there.
    The mapping of the index will be:
                - url: url with information
                - channel: dictionary with name of the channel and list1
                            list with the ids of the messages containig
                            that url

    Parameters:
       client -> elasticSearch client

   """

    logging.info("Creating index %s", name)

    client.indices.create(
        index=name,
        body={
            "settings": {
                "number_of_shards": 1,
                "index.mapping.total_fields.limit": 6000,
                "index.mapping.depth.limit": 100},
            "mappings": {
                "properties": {
                    "url": {
                        "type": "text"},
                    "title": {
                        "type": "keyword"},
                    "text": {
                        "type": "text"},

                    "channel": {
                        "type": "object"
                    },
                    "twitter": {
                        "type": "object"
                    },
                    "attemps": {
                        "type": "integer"
                    },
                    "date": {
                        "type": "date",
                    }}},
        },
        ignore=400,
    )

    logging.info("Index created")


def create_index_old(client, name):
    """
    Creates an index in Elasticsearch if one isn't already there.
    The mapping of the index will be:
                - url: url with information
                - channel: dictionary with name of the channel and 
                            list with the ids of the messages containig
                            that url

    Parameters:
       client -> elasticSearch client

   """

    logging.info("Creating index %s", name)
    client.indices.create(
        index=name,
        body={
            "settings": {
                "number_of_shards": 1,
                "index.mapping.total_fields.limit": 6000,
                "index.mapping.depth.limit": 100},
            "mappings": {

                "properties": {
                    "url": {
                        "type": "text"},
                    "title": {
                        "type": "keyword"},
                    "text": {
                        "type": "keyword"},
                    "channel": {
                        "type": "object"},
                    "twitter": {
                        "type": "object"},
                    "attemps": {
                        "type": "integer"},
                    "date": {
                        "type": "date"},
                    "date_delete": {
                        "type": "date"}}},
        },
        ignore=400,
    )

    logging.info("Index created")


def run_elasticSearch():
    """
    Reads the data sent by the consumer. It will check if for each
    url, this url is already in the database. In this case, it will add
    the channel with the id of the message that contains the url.
    Otherwise, it will created a  new entry for the url with the
    corresponding data.

    Parameters:
        client -> elasticSearch client

   """

    ddbb_data = None

    for message in consumer:
        message = message.value
        logging.info("Insert %s", str(message))
        for item in message:

            api = None
            if item is not None:
                for url in item.keys():

                    data_to_append = None
                    ddbb_data = None
                    old_tweets = {}

                    with mutex:
                        # We check if there is already an entry in the index
                        # with that url and id
                        try:
                            messages = client.search(index=config_data['INDEX_NAME_ACTIVE'], body={
                                "query": {"match": {"url": str(url)}}})
                        except:
                            logging.error("Error in ES when searching")

                        id = None
                        if len(messages['hits']['hits']) != 0:
                            # We find the id of the url in the ddbb
                            for list2 in messages['hits']['hits']:
                                if url == list2['_source']['url']:
                                    id = list2['_id']
                                    break

                            if id is not None:
                                ddbb_data = messages['hits']['hits'][0]['_source']['channel']
                                old_tweets = messages['hits']['hits'][0]['_source']['twitter']
                                attemps = messages['hits']['hits'][0]['_source']['attemps']
                                date_url = messages['hits']['hits'][0]['_source']['date']
                                title = messages['hits']['hits'][0]['_source']['title']
                                text = messages['hits']['hits'][0]['_source']['text']

                        # If the url is already in the ddbb
                        if ddbb_data is not None:

                            # We get only the different elements
                            new_channels = list(
                                set(item[url].keys()) ^ set(ddbb_data.keys()))

                            for channel in new_channels:
                                try:
                                    update_channels(client, channel, url)
                                except:
                                    logging.error("Error in updating channels")
                            data_to_append = combine_dicts(
                                item[url], ddbb_data)

                            body = {"doc": {
                                'url': str(url),
                                'title': title,
                                'channel': data_to_append,
                                'twitter': old_tweets,
                                'attemps': attemps,
                                'text': text,
                                'date': date_url
                            }}
                            try:
                                client.update(
                                    index=config_data['INDEX_NAME_ACTIVE'],
                                    id=id,
                                    body=body)

                            except:
                                logging.error("Error in ES when update")

                            continue
                        
                        # Get tweets of url
                        tweets = {}
                        try:
                            tweets = get_tweets(api, url, True)
                        except:
                            logging.error("Error in ES when getting tweets")

                        # Updates channels stadistics
                        for channel in item[url]:
                            try:
                                update_channels(client, channel, url)
                            except:
                                logging.error("Error adding channel")

                        
                        # Get title of the news
                        title = ""
                        try:
                            title = get_title(url)
                        except:
                            title = "NEWS"

                        # Get body of the news
                        text = ""
                        try:
                            text = get_text(url)
                        except:
                            text = ""

                        try:
                            client.index(
                                index=config_data['INDEX_NAME_ACTIVE'],
                                body={
                                    'url': str(url),
                                    'title': title,
                                    'channel': item[url],
                                    'twitter': tweets,
                                    'text': text,
                                    'attemps': 0,
                                    'date': datetime.now() + timedelta(hours=2)})
                        except:
                            logging.error("Error in ES when insert")


def run_twitter():

    while (True):
        with mutex:
            logging.info("Update tweets")
            api = None

            # We search for all the data in the database
            try:
                messages = client.search(
                    index=config_data['INDEX_NAME_ACTIVE'],
                    size=1500)
            except:
                logging.error("Error in ES when searching for update")

            for message in messages['hits']['hits']:
                try:
                    update_tweets(args.config, client, api, message)
                except:
                    time.sleep(60 * 3)  # Sleep for 3 minutes

        time.sleep(60 * 60 * 3)


def run_topics():

    while (True):

        with mutex:
            logging.info("Update topics")

            if (get_trending_topics(client, config_data['INDEX_TOPICS']) is False):
                logging.error("Error updating trending topics")

        time.sleep(60 * 60 * 2)


if __name__ == "__main__":

    time.sleep(60)
    logging.info("Begin")
    # Establish connection
    client = Elasticsearch(
        hosts=[{"host": "example_es"}], port=9200
    )

    logging.info("Connection with ES done")

    # If index of active items is not created yet, we create it
    if not client.indices.exists(index=config_data['INDEX_NAME_ACTIVE']):
        create_index(client, config_data['INDEX_NAME_ACTIVE'])

    # If index of no active items is not created yet, we create it
    if not client.indices.exists(index=config_data['INDEX_NAME_NO_ACTIVE']):
        create_index_old(client, config_data['INDEX_NAME_NO_ACTIVE'])

    # If index of channels is not created yet, we create it
    if not client.indices.exists(index=config_data['INDEX_CHANNELS']):
        create_index_channels(client, config_data['INDEX_CHANNELS'])

     # If index of TOPICS is not created yet, we create it
    if not client.indices.exists(index=config_data['INDEX_TOPICS']):
        create_index_topics(client, config_data['INDEX_TOPICS'])

    t1 = threading.Thread(target=run_elasticSearch).start()
    time.sleep(60*40)
    logging.info("Twitter starts")
    t2 = threading.Thread(target=run_twitter).start()
    time.sleep(60*5)
    logging.info("Topics starts")
    t3 = threading.Thread(target=run_topics).start()
