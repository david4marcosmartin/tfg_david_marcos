

async def check_url(url):
    """
    Funtion to check if the url is valid

    Parameters:
       url -> url
   Return:
       True if the url is valid, False otherwise
   """
    # Remove telegram profiles
    if url.startswith('http://bit.ly/') or url.startswith('https://bit.ly/'):
        return False
    # Remove twitter profiles
    elif url.startswith('http://t.me/') or url.startswith('https://t.me/'):
        return False
    elif url.startswith('https://mobile.twitter.com/') or \
            url.startswith('http://mobile.twitter.com/'):
        return False
    # Remove twitch profiles
    elif url.startswith('https://twitch.tv/') or url.startswith('https://twitch.tv/'):
        return False
    # Remove youtube profiles
    elif url.startswith('http://is.gd/') or url.startswith('https://is.gd/'):
        return False
    # Remove tiktok profiles
    elif url.startswith('http://www.tiktok.com/') or \
            url.startswith('https://www.tiktok.com/'):
        return False
    else:
        return True
