import argparse
import json
import logging
import os
import re
import sys
from argparse import RawTextHelpFormatter
from datetime import datetime
from parse_url import check_url
import time
import random
from configuration import *
from kafka import KafkaProducer
from telethon import TelegramClient
from telethon.errors import SessionPasswordNeededError
from telethon.tl.functions.messages import (GetHistoryRequest)

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',filename='telegram.log',filemode='a+',  level=logging.INFO)


counter_num = 0

# some functions to parse json date
class DateTimeEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()

        if isinstance(o, bytes):
            return list(o)

        return json.JSONEncoder.default(self, o)
    
config_data = None
data_telegram = None
client = None
       

# Parse of arguments
parser = argparse.ArgumentParser(
    description='Reading Telegram channel messages via channel URL',
    formatter_class=RawTextHelpFormatter)
parser.add_argument(
    '--config',
    dest='config',
    default=False,
    help='Configuration file')

args = parser.parse_args()

if args.config is False:
    parser.print_help()
    sys.exit(-1)



# Create producer to send url messages
producer = KafkaProducer(bootstrap_servers=['kafka1:29092'],
                         value_serializer=lambda x:
                         json.dumps(x).encode('utf-8'),
                         api_version=(0, 11, 5),
                         request_timeout_ms=10000000
                        


                         )


async def query(my_channel, offset_id=0, offset_date=None, add_offset=0,
                limit=0, max_id=0, min_id=0, hash=0):
    """
    Funtion to download channel information with
    certain filters

    Parameters:
       my_channel -> Telegram channel
       offset_id -> ID of the message to use as offset
       add_offset -> Additional offset
       offset_date -> Date of the message to use as offset
       limit -> number of messages to download
       max_id -> Maximum message ID
       min_id -> Minimum message ID

   Return:
       List with the downloaded messages.
   """

    messages = None
    
    history = None

    try:
        history = await client(GetHistoryRequest(
            peer=my_channel,
            offset_id=offset_id,
            offset_date=offset_date,
            add_offset=add_offset,
            limit=limit + 1,
            max_id=max_id,
            min_id=min_id,
            hash=hash

        ))
        
    except:
        logging.error("Telegram error")
        return None
    
    if not history.messages:
        return None

    messages = history.messages

    return messages


async def get_urls(message_dict, channel_name):
    global counter_num
    """
    Funtion to get the urls of a message

    Parameters:
       message_dict -> dictionary with messages
       channel_name -> channel name

   Return:
        Dictionary with the urls of each message
            key : id of message
            value: urls in the message
   """
    if message_dict.get("message"):

        # Gets all urls in the message
        urls_list = re.findall(r'(https?://\S+)', message_dict["message"])
        urls = {}

        # Adds the urls in the dictionary
        for url in urls_list:
            # Check if it is a valid url
            if await check_url(url):

                # If url is already in the dictionary
                if urls.get(url):
                    # If the channel is already en the dictionary, in this url
                    if (urls.get(url)).get(channel_name):
                        # We check if the id of the message is in the list
                        if message_dict["id"] not in (
                                (urls.get(url))[channel_name]):
                            ((urls.get(url))[channel_name]).append(
                                message_dict["id"])
                    # We add a new entry in the values of the url with the
                    # channel and the id of the message
                    else:
                        (urls.get(url))[channel_name] = [message_dict["id"]]
                # If url is nor already in the dictionary, we add a new entry
                else:
                    counter_num += 1
                    urls[url] = {channel_name: [message_dict["id"]]}
        return urls


async def get_latest_messages(my_channel, limit, id_old_final, channel_name):
    """
    Funtion to download the N latest messages of the channel

    Parameters:
       my_channel -> Telegram channel
       limit -> number of messages to download

   Return:
        List with the downloaded messages.
   """

    is_ini = False
    urls_list = list()

    messages = await query(my_channel=my_channel, limit=limit)

    if messages is None:
        return None

    # We add messages to list
    for message in messages:
        message_dict = message.to_dict()

        # We update id_ini, latest id message
        if is_ini is False:
            id_ini = message_dict["id"]
            is_ini = True
        # id final will be the oldest id message
        id_final = message_dict["id"]

        # We get the urls of the messages
        urls = await get_urls(message_dict, channel_name)
        urls_list.append(urls)

    # Check if we have oldest messages stored
    if id_old_final > 0 and id_old_final < id_final:
        id_final = id_old_final

    message = {"id_ini": id_ini, "id_final": id_final}
    
    try:
        producer.send(config_data['PIPELINE1'], value=urls_list)
        producer.flush()
    except:
        logging.error("Kafka error")
    
    message_str = str(urls_list)
    logging.info("Send %s", message_str)

    return message


async def main(phone):
    await client.start()

    # Ensure you're authorized
    if await client.is_user_authorized() is False:
        await client.send_code_request(phone)
        try:
            await client.sign_in(phone, input('Enter the code: '))
        except SessionPasswordNeededError:
            logging.error("Auth error")
            await client.sign_in(password=input('Password: '))

    await client.get_me()
    
    logging.info("Authetication completed")

    # Read file with url of telegram channel
    try:
        f = open(args.file, "r")
    except FileNotFoundError:
        logging.error("File %s not found",args.file )
        sys.exit(-1)

    data_messages = None
    if os.path.isfile(config_data['MESSAGE_FILE']) is True:
        file_messages = open(config_data['MESSAGE_FILE'], "r")
        json_data = file_messages.read()
        data_messages = json.loads(json_data)

    # For each channel
    for url in f:
        # we get the name channel, which is at the end of the url
        telegram_name = url.split("/")[-1]
        message = None

        # Avoid empty lines in file
        if len(telegram_name) <= 1:
            continue

        if telegram_name[-1] == "\n":
            telegram_name = telegram_name[:-1]

        # If the channel does not exist we continue
        try:
            my_channel = await client.get_entity(telegram_name)
        except ValueError:
            continue

        if data_messages is not None and data_messages.get(telegram_name):
            data = data_messages[telegram_name]

            # Obtain initial and last id of the messages already downloaded
            id_ini = int(data['id_ini'])
            id_final = int(data['id_final'])

            # Obtain last message sended
            messages = await query(my_channel=my_channel, limit=1)

            if messages is None:
                continue

            # Get id of last message sended
            message_id = messages[0].to_dict()["id"]

            # If last message id of the channel is equal to the last message id
            # we have downloaded, we are updated with the new messages
            if id_ini == message_id:
                continue

            # If there are new messages not downloaded in the chat
            # and there are more newest messages then the ones we want to
            # download
            elif id_ini < message_id:
                # We remove channel data from dictionary to add the new data
                data_messages.pop(telegram_name)

                limit = message_id - id_ini

                message = await get_latest_messages(my_channel,
                                                    limit, id_final,
                                                    telegram_name)

        # If the file does not exit, we download the latest num_messages
        # mesagges
        else:
            # Set number of messages to download
            limit = int(args.num_messages)

            message = await get_latest_messages(my_channel, limit, 0,
                                                telegram_name)

        if message is None:
            continue

        if data_messages is None:
            data_messages = {}

        data_messages[telegram_name] = message

        # We write the messages downloaded in the corresponding file
        if os.path.isdir('messages') is False:
            os.mkdir('messages')

        with open(config_data['MESSAGE_FILE'], 'w') as outfile:
            json.dump(data_messages, outfile, cls=DateTimeEncoder)

i = 0

def data():
    global i
    URLS_DIR = 'urls/'
    urls_lis = ['url.txt', 'url1.txt']

    
    random.seed = os.urandom(128)
    args.account =  str(random.randint(1, 5))
    args.file = URLS_DIR + urls_lis[i]
    i += 1
    if i == 2:
        i = 0
    args.num_messages = random.randint(20, 30)


# Initial configuration
data()
config_data = credentails(args.config)

data_telegram = credentails_telegram(args)

phone = data_telegram['phone']

# Create the client and connect
client = TelegramClient(
        data_telegram['username'],
        data_telegram['api_id'],
        data_telegram['api_hash'])

with client:
    
    while True:
        # Execution
        client.loop.run_until_complete(main(phone))
        # Close connection
        client.disconnect()
        # Change configuration
        data()
        
        config_data = credentails(args.config)

        data_telegram = credentails_telegram(args)

        phone = data_telegram['phone']

        # Create the client and connect
        client = TelegramClient(
                data_telegram['username'],
                data_telegram['api_id'],
                data_telegram['api_hash'])
        
        # Wait until next execution
        time.sleep(60 * random.randint(120, 150) )
