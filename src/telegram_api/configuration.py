import argparse
import configparser
import json


def credentails(config_file):
    """
    Reads the data from config file 
    
    Parameters:
        config_file -> configuration file
        
    Return:
        Dictionary with data from file
    """
    
    # Reading Configs
    config = configparser.ConfigParser()
    config.read(config_file)
    

    # Read credentials from file
    MESSAGE_FILE = str(config['Data']['MESSAGE_FILE'])
    PIPELINE1 = str(config['Data']['PIPELINE1'])
    INDEX_NAME_ACTIVE = str(config['Data']['INDEX_NAME_ACTIVE'])
    INDEX_NAME_NO_ACTIVE = str(config['Data']['INDEX_NAME_NO_ACTIVE'])
    INDEX_CHANNELS = str(config['Data']['INDEX_CHANNELS'])
    INDEX_TOPICS = str(config['Data']['INDEX_TOPICS'])
    URLS_FILE = str(config['Data']['URLS_FILE'])
   
    return {'MESSAGE_FILE': MESSAGE_FILE, 'PIPELINE1': PIPELINE1,
            'INDEX_NAME_ACTIVE': INDEX_NAME_ACTIVE , 'INDEX_NAME_NO_ACTIVE': INDEX_NAME_NO_ACTIVE ,
            'URLS_FILE': URLS_FILE,  'INDEX_CHANNELS': INDEX_CHANNELS, 'INDEX_TOPICS': INDEX_TOPICS }


def credentails_telegram(args):
    """
    Reads the data from config file and makes the authentification
    in telegram api

    Parameters:
        args -> args parameters
    
    Return:
        Dictionary with data from file
    """
    
    # Reading Configs
    config = configparser.ConfigParser()
    config.read(args.config)
    
  
    if args.account is False:
        args.account = 1
     # Reading Configs
     
    account = 'Telegram' + str(args.account)

    # Setting configuration values
    api_id = config[account]['api_id']
    api_hash = config[account]['api_hash']

    api_hash = str(api_hash)

    phone = config[account]['phone']
    username = config[account]['username']
      
    return {'api_id': api_id, 'api_hash': api_hash, 'phone' : phone, 'username': username}

    

def credentails_twitter(config_file):
    """
    Reads the data from config file and makes the authentification
    in twitter api

    Parameters:
        config_file -> configuration file
    
    Return:
        Dictionary with data from file
    """
    
    config_list = list()
    # Reading Configs
    config = configparser.ConfigParser()
    config.read(config_file)

    # Read credentials from file
    CONSUMER_KEY = config['Twitter']['CONSUMER_KEY']
    CONSUMER_SECRET = config['Twitter']['CONSUMER_SECRET']
    ACCESS_TOKEN = config['Twitter']['ACCESS_TOKEN']
    ACCESS_TOKEN_SECRET = config['Twitter']['ACCESS_TOKEN_SECRET']
    
    config_list.append({'CONSUMER_KEY': CONSUMER_KEY, 'CONSUMER_SECRET': CONSUMER_SECRET, 'ACCESS_TOKEN' : ACCESS_TOKEN, 'ACCESS_TOKEN_SECRET': ACCESS_TOKEN_SECRET})
    
    # Read credentials from file
    CONSUMER_KEY = config['Twitter1']['CONSUMER_KEY']
    CONSUMER_SECRET = config['Twitter1']['CONSUMER_SECRET']
    ACCESS_TOKEN = config['Twitter1']['ACCESS_TOKEN']
    ACCESS_TOKEN_SECRET = config['Twitter1']['ACCESS_TOKEN_SECRET']
    
    config_list.append({'CONSUMER_KEY': CONSUMER_KEY, 'CONSUMER_SECRET': CONSUMER_SECRET, 'ACCESS_TOKEN' : ACCESS_TOKEN, 'ACCESS_TOKEN_SECRET': ACCESS_TOKEN_SECRET})
   
    return config_list