from elasticsearch import Elasticsearch
from datetime import datetime, timedelta


def get_trending_topics(client, index):
    """
    Gets the trending topics in the active news

    Parameters:
        client ->  ElasticSearch API
        index -> index name

    Return:
        True if there is a correct execution, false otherwise
    """

    data = {}

    # Gets all active news
    results = client.search(index="active",  size=10000)['hits']['hits']

    for res in results:
        try:
            channels = list(res['_source']['channel'].keys())

            # Obtains trending topics in the body of the news
            term_vectors = client.termvectors(index="active",  id=res['_id'], offsets=True,
                                              payloads=True, positions=True, fields='text',
                                              field_statistics=True
                                              )['term_vectors']['text']['terms']

            for item in term_vectors:
                freq = int(term_vectors[item]['term_freq'])
                # Checks the puntuation of the topic
                if int(term_vectors[item]['term_freq']) > 30:
                    if(len(item) > 4 and item.isalpha()):
                        try:
                            values = data[item]
                        except KeyError:
                            values = data[item] = []

                            channels_text = ""
                            for channel in channels:
                                channels_text += channel + ", "

                            channels_text = channels_text[:-2]
                            values.append(
                                {'topic': item, 'channels': channels_text, 'freq': freq})

        except KeyError:
            pass

    try:
        client.index(
            index=index,
            body={
                'topics': data,
                'date': datetime.now() + timedelta(hours=2)
            })
        return True
    except:
        return False
