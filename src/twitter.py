import tweepy
import json
from datetime import timedelta, datetime
import time
import sys
import argparse
import random
import logging
import configuration as cf


client = None

config_data = cf.credentails('config/config.ini')


def auth(config_file):
    """
    Reads the data o the configutation file to obain the authentification
    in twitter api

    Parameters:
        config_file -> configuration file

    Returns:
        Twitter Client API
    """

    twitter_data = cf.credentails_twitter(config_file)

    api_list = list()

    for data in twitter_data:

        try:
            # Authentification
            auth = tweepy.OAuthHandler(
                data['CONSUMER_KEY'], data['CONSUMER_SECRET'])
            auth.set_access_token(
                data['ACCESS_TOKEN'], data['ACCESS_TOKEN_SECRET'])
            
        
            api = tweepy.API(auth)

            api_list.append(api)
        except:
            logging.error("Error in Twitter when auth")
        

    return api_list[0], api_list[0]


def get_api():
    api1, api2 = auth('config/config.ini')
    
    if random.randint(1, 2) == 1:
        return api1
    else:
        return api2


def update_channels_old(client, channel, url):
    """
    Adds one to the olds news in the channel index

    Parameters:
        client -> ElasticSearch client
        channel -> channelto be updated
    """

    try:
        messages = client.search(index=config_data['INDEX_CHANNELS'], body={
            "query": {"match": {"channel": str(channel)}}})
    except:
        logging.error("Error in ES when searching for update num channels old")
 
    if len(messages['hits']['hits']) != 0:
        id = None
        news = messages['hits']['hits'][0]['_source']['news']
        
        if url in news:
            news[url]['active'] = False
            
        for list in messages['hits']['hits']:
            if channel == list['_source']['channel']:
                id = list['_id']
                break
        if id is not None:

            print(client.update(
                index=config_data['INDEX_CHANNELS'],
                id=id,
                body={"doc": {
                    'channel': str(channel),
                    'news': news}}), flush=True)


def parse_url(url):
    """
    Changes the url downloaded from telegram into the format
    of the query of tweetpy

    Parameters:
        url -> url downloaded from telegram

    Returns:
        Parsed url
    """

    url = url.replace("https://", "")
    url = url.replace("/", "%2F")
    return url


def search_tweets(api, query, is_new=False):
    """
    Gets the tweets that contain a specific url

    Parameters:
        api ->  Twitter API
        query ->  query with url to find in twitter

    Returns:
        Tweets conaining the url
    """
    
    tweets_search = {}
    # Change proxy for twitter API in docker
    api_r = get_api()
   
    if api_r is None:
        logging.error("API is none")
        
    
    try:
        tweets_search = tweepy.Cursor(
            api_r.search_tweets, query, result_type='popular').items()
    except:
        logging.warn("Error in twitter, waiting")
        time.sleep(60*10)
       
    return tweets_search


def get_quoted_tweets(api, id):
    """
    Gets quoted tweets of a tweets, with its id

    Parameters:
        api ->  Twitter API
        id ->  id of the tweet to find in twitter

    Returns:
        Quoted tweets of a certain twetd with a specific id
    """
    tweets = {}
    tweet_search = search_tweets(api, id)
    for tweet in tweet_search:
        tweet = tweet._json
        if str(id) != str(tweet["id"]):
            profile_image = tweet['user']['profile_image_url_https']
            profile_name = tweet['user']['name']
            tweets[tweet["id"]] = {
                'retweet': tweet['retweet_count'], 'favorites': tweet['favorite_count'],
                'profile_image': profile_image, 'profile_name': profile_name, 'text': tweet['text']}


    return tweets


def is_tweet(url):
    # Remove telegram profiles
    if url.startswith('https://twitter.com/'):
        return True
    else:
        return False

def get_title_twitter(url):
    api_r = get_api()
    url = url.split("/")[-1]
    
    try:
        tweets_search = api_r.get_status(url)
    except:
        return "Tweet"
    title = tweets_search._json['text']
    title = title.split("https://t.co/")[0]
   
    return title

def get_tweets(api, url, is_new=False):
    """
    Gets the tweets of a given URL

    Parameters:
        api ->  Twitter API
        url ->  url to find in twitter

    Returns:

        Recent(last 7 days) and popular tweets containig the url
    """

    tweets = {}
    if is_tweet(url):
        id = url.split("/")[-1]
        quoted_tweets = get_quoted_tweets(api, id)

        for tweet in quoted_tweets:

            tweets[tweet] = {'retweet': quoted_tweets[tweet]['retweet'],
                             'favorites': quoted_tweets[tweet]['favorites'],
                             'profile_image': quoted_tweets[tweet]['profile_image'],
                             'name': quoted_tweets[tweet]['profile_name'],
                             'text': quoted_tweets[tweet]['text'],
                             'quoted_tweets': {}}

        return tweets
    
    tweets_search = search_tweets(api, parse_url(url), is_new)

    # We get the popular tweets in the last 7 days containing the urk
    for tweet in tweets_search:
        tweet = tweet._json
        quoted_tweets = get_quoted_tweets(api, tweet["id"])
          
        profile_image = tweet['user']['profile_image_url_https']
        profile_name = tweet['user']['name']

        tweets[tweet["id"]] = {'retweet': tweet['retweet_count'], 'text': tweet['text'],
                               'favorites': tweet['favorite_count'], 'quoted_tweets': quoted_tweets,
                               'name': profile_name, 'profile_image': profile_image}

    return tweets


def update_tweets(config_file, client, api, message):
    """
    Updates the tweets of each url in the database.
    If we have tried to update the tweets of an url
    for 6 times without been possible, we will remove
    this url from the database.

    Parameters:
        api ->  Twitter API
        client ->  Telegram API
        config_file -> configuration file
        message -> message with information

    """

    if message is not None:

        # Obtain data from message
        id = message['_id']
        url = message['_source']['url']
        title = message['_source']['title']
        ddbb_data = message['_source']['channel']
        date_url = message['_source']['date']
        attemps = message['_source']['attemps']
        tweets = message['_source']['twitter']
        text = message['_source']['text']

        insert_tweets = get_tweets(api, url)
        
       
        if len(insert_tweets) != 0:
            # If we have the same tweets as before
            if insert_tweets == tweets:
                attemps += 1
            else:
                attemps = 0

            body = {"doc": {
                'url': str(url),
                'title': title,
                'channel': ddbb_data,
                'twitter': insert_tweets,
                'attemps': attemps,
                'text': text,
                'date': date_url
            }}

        else:
            # If there are no popular tweets
            attemps += 1
            # If we have reached the limit, we do not add the url
            if attemps == 8:
              
                for channel in ddbb_data:
                    update_channels_old(client, channel, url)

                # Remove data from active index
                client.delete(index=config_data['INDEX_NAME_ACTIVE'],
                              id=id)

                # We remove the item from the active ones, and add it to the no active items
                body = {"doc": {
                    'url': str(url),
                    'title': title,
                    'channel': ddbb_data,
                    'twitter': tweets,
                    'attemps': attemps,
                    'text': text,
                    'date': date_url,
                    'date_delete': datetime.now() + timedelta(hours=2)
                }}

                client.index(index=config_data['INDEX_NAME_NO_ACTIVE'],

                             body=body)

                return
            else:
                body = {"doc": {
                    'url': str(url),
                    'channel': ddbb_data,
                    'twitter': tweets,
                    'attemps': attemps,
                    'date': date_url
                }}
        client.update(
            index=config_data['INDEX_NAME_ACTIVE'],  id=id, body=body)
