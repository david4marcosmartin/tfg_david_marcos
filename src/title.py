from urllib.request import urlopen
from html.parser import HTMLParser
from newspaper import Article
from lxml.html import parse

from twitter import *

NO_TITLE = "NEWS"


class TitleParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.match = False
        self.title = ''

    def handle_starttag(self, tag, attributes):
        self.match = True if tag == 'title' else False

    def handle_data(self, data):
        if self.match:
            self.title = data
            self.match = False


def get_title(url):
    """
    Given a URL, returns the title of the web page

    Parameters:
        URL ->  URL 

    Returns:
        Title of the web page of the URL

    """

    title = ""

    if is_tweet(url):
        title = get_title_twitter(url)

    try:
        title = get_title_news(url)
    except:
        title = title2(url)

    return title if len(title) > 0 else NO_TITLE


def title2(url):
    """
    Given a URL, returns the title of the web page, way 2

    Parameters:
        URL ->  URL 

    Returns:
        Title of the web page of the URL

    """
    try:
        page = urlopen(url)
        p = parse(page)
        if p.find(".//title") is None:
            return NO_TITLE
        else:
            return p.find(".//title").text
    except:
        return NO_TITLE


def get_title_news(url):
    """
    Given a URL, returns the title of the web page

    Parameters:
        URL ->  URL 

    Returns:
        Title of the web page of the URL
    """
    article = Article(url)
    article.download()
    article.parse()
    return article.title


def get_text(url):
    """
    Given a URL, returns the body text of the web page

    Parameters:
        URL ->  URL 

    Returns:
        Body of the web page of the URL
    """

    article = Article(url)
    article.download()
    article.parse()

    return article.text
