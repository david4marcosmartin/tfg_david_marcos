# tfg_david_marcos

This project has been developed for the final degree project of the student David Marcos Martin from the
Universidad Autonoma de Madrid. This project has been mentored by Pablo Haya,



Abstract
=======

Disinformation is a social problem that is becoming particularly relevant in recent times. There has been a proliferation of media outlets
that publish fake information by simulating the publication methods of reliable media outlets, ignoring verification processes or intentionally
distorting the content. Information pluralism is seriously damaged when disinformation campaigns confuse the public opinion, making it difficult to
discern what is true or false. Through social networks, this news becomes viral faster than truthful news, impacting a larger number of people. 

The goal of this Final Degree Project is to develop a computer system that automatically detects news that are viralizing on the social network Twitter,
that have been published in digital sources that have been previously identified as unreliable, or tend to spread disinformation. For this purpose,
a system has been created that will constantly collect Telegram posts coming from channels suspected of spreading fake news. These posts will be analyzed
to obtain the URL contained in these posts. Subsequently, a study will be carried out on Twitter to monitor their virality. The outcome of this
monitoring will be visualized in real time through a user interface accessible via web.

As a result of this computer system, we have obtained an accessible web where we can visualize the news disseminated by these channels that have reached
Twitter and have had some iteration, either with number of retweets or favorites.  In addition, we can observe statistics on the number of news published
every hour, as well as the channels that publish the most news. Moreover, we can also check the current topics of these news items.



License
=======

The package is licensed under the BSD 3-Clause License. A copy of the
license_ can be found along with the code.

.. _license: https://bitbucket.org/david4marcosmartin/tfg_david_marcos/src/9f520c811c679c37b1a4e7c7caa98dc56aaaba76/LICENSE.txt?at=develop